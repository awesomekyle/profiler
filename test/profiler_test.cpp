/** @file math_test.cpp
 *  @copyright Copyright (c) 2013 Kyle Weicht. All rights reserved.
 */
#include "unit_test.h"

#include "profiler.h"

namespace
{

TEST(ProfilerReset)
{
    profile_reset();
}

} // anonymous namespace

/** @file profiler.h
 *  @brief Main profiler API
 *  @copyright Copyright (c) 2013 Kyle Weicht. All rights reserved.
 */
#ifndef __profiler_h__
#define __profiler_h__

#ifdef __cplusplus
    extern "C" {
#endif

void profile_reset(void);

#ifdef __cplusplus
    } // extern "C" {
#endif

#endif /* include guard */
